### About
This is my I3 config.

### Fonts

 1. [Ubuntu](http://font.ubuntu.com/)
 2. [Octicons](https://octicons.github.com/) 

### TODO:

 * Replace i3bar to i3blocks
 * Add keyboard layout widget
 * Add xinit config (keyboard layout section)
 * Modify fonts settings
 * Replace i3lock to gnome screensaver
